angular.module('starter.controllers', [])


// A simple controller that fetches a list of data from a service
.controller('LinhaIndexCtrl', function($scope, LinhaService) {
  // "Linhas" is a service returning mock data (services.js)
  $scope.linhas = LinhaService.all();
})

.controller('LinhaIndexCtrlPrx', function($scope, LinhaService) {
  // "Linhas" is a service returning mock data (services.js)
  $scope.linhas = LinhaService.get_near();
})


// A simple controller that shows a tapped item's data
.controller('LinhaDetailCtrl', ['$scope', '$stateParams', 'LinhaService',
            function ($scope, $stateParams, LinhaService) {
  // "Linhas" is a service returning mock data (services.js)
  function init () {
  	var map;
    var mapOptions = {
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
    };
    map = new google.maps.Map(document.getElementById('map'),
      mapOptions);
    $scope.map = map;

    // Capturando a localização HTML5
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);

        var infowindow = new google.maps.InfoWindow({
          map: map,
          position: pos,
          content: 'Você está aqui.'
        });

        map.setCenter(pos);
      });
    } 

    // pontos_linha = LinhaService.get_points($stateParams.linhaId);
    var flightPlanCoordinates = LinhaService.get_points($stateParams.linhaId);


    // var j = 0;
    // for (var i = 1; i <= pontos_linha.length - 1; i = i + 2) {
    //     gmp = new google.maps.LatLng( pontos_linha[i], pontos_linha[i-1] );
    //     flightPlanCoordinates[j] = gmp;        
    //     j++;
    // }; 
    
    // console.log(gmp);

// 

    console.log(flightPlanCoordinates);
      var flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
      });

      flightPath.setMap(map);


  }

$scope.centerOnMe = function() {
  	
    navigator.geolocation.getCurrentPosition(function(pos) {
          $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        }, function(error) {
          alert('Unable to get location: ' + error.message);
        });
  

  }


function marcador (){
  
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var myLatlng = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
      var marker = new google.maps.Marker({
        position: myLatlng,
        map: $scope.map,
      });
    })
  }
}  

  init();
  marcador();
  

}]);


