// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.services', 'starter.controllers'])


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tab.linhas-index', {
      url: '/linhas',
      views: {
        'linhas-tab': {
          templateUrl: 'templates/linhas-index.html',
          controller: 'LinhaIndexCtrl'
        }
      }
    })

    .state('tab.linhas-detail', {
      url: '/linha/:linhaId',
      views: {
        'linhas-tab': {
          templateUrl: 'templates/linhas-detail.html',
          controller: 'LinhaDetailCtrl'
        }
      }
    })

    .state('tab.linhas-proximas-index', {
      url: '/proximos',
      views: {
        'linhas-proximas-tab': {
          templateUrl: 'templates/linhas-proximas-index.html',
          controller: 'LinhaIndexCtrlPrx'

        }
      }
    })

    .state('tab.about', {
      url: '/about',
      views: {
        'about-tab': {
          templateUrl: 'templates/about.html'
        }
      }
    });

  // se nenhum dos estados acima for marcado, é usado a url abaixo
  $urlRouterProvider.otherwise('/tab/linhas');

});

